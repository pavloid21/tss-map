$(document).ready(function(){
    $.ajax({
        type: 'GET',
        url: 'markers.xml',
        datatype: 'xml',
        success: xmlParser
    });
    $('#table').fadeIn(1700);
    $('thead').append('<tr><th>Наименование МО</th><th>Общее кол-во АРМ, подключенных к РФ ЕГИСЗ</th><th>АРМ медицинских работников поликлиники</th><th>АРМ медицинских работников стационара</th><th>АРМ прочих специалистов МО, работающих в РФ ЕГИСЗ</th></tr>');

});

function xmlParser(xml){
    var summ1 = 0;
    var summ2 = 0;
    var summ3 = 0;
    var summ4 = 0;
    var awpCount = [];
    var awpClinicCount = [];
    var awpHospCount = [];
    var awpOtherStaffCount = [];
    $(xml).find('marker').each(function(){
        awpCount.push($(this).find('awpCount').text());
        awpClinicCount.push($(this).find('awpClinicCount').text());
        awpHospCount.push($(this).find('awpHospCount').text());
        awpOtherStaffCount.push($(this).find('awpOtherStaffCount').text());
        $('tbody').append('<tr><td>'+$(this).find('name').text()+'</td><td>'+ $(this).find('awpCount').text() +'</td><td>' + $(this).find('awpClinicCount').text() + '</td><td>' + $(this).find('awpHospCount').text() + '</td><td>' + $(this).find('awpOtherStaffCount').text() + '</td></tr>');
    });

    for (var i = 0;i<awpCount.length;i++){
        summ1 += parseInt(awpCount[i]) || 0;
        summ2 += parseInt(awpClinicCount[i]) || 0;
        summ3 += parseInt(awpHospCount[i]) || 0;
        summ4 += parseInt(awpOtherStaffCount[i]) || 0;
    }
    $('tfoot').append('<tr><th>Итого: </th><th>'+summ1+'</th><th>'+summ2+'</th><th>'+summ3+'</th><th>'+summ4+'</th></tr>');
    var ctx = $('#myChart');
    var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: [((100*summ2)/summ1).toFixed(2)+"% АРМ мед. работников поликлиники", ((100*summ3)/summ1).toFixed(2)+"% АРМ мед. работников стационара", ((100*summ4)/summ1).toFixed(2)+"% АРМ прочих специалистов МО", ((100*(summ1-summ2-summ3-summ4))/summ1).toFixed(2)+"% Остальные АРМ"],
        datasets: [{
            data: [summ2, summ3, summ4, (summ1 -summ2 - summ3 - summ4)],
            backgroundColor: [
                'rgba(255, 99, 132, 0.5)',
                'rgba(54, 162, 235, 0.5)',
                'rgba(255, 206, 86, 0.5)',
                'rgba(75, 192, 192, 0.5)'
            ],
            hoverBackgroundColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)'
            ]
        }]
    }
});
}
