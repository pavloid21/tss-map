$(document).ready(function() {
  var names = [];
  $("#search").click(function(){
     $.get('markers.xml', function(data){
        var flag = 0;
           a = $(data).find('marker').each(function(){
                  var name = $(this).find('name').text();
                  var search = document.getElementById("search_word");

                        if (name.indexOf(search.value) > -1){
                            var name = $(this).find('name').text();
                            var lat = $(this).find('lat').text();
                            var lng = $(this).find('lng').text();
                            myMap.setCenter([parseFloat(lat), parseFloat(lng)], 16, {checkZoomRange: true});
                            names.push(name);
                            flag++;
                        }

                    });
                    if (flag > 1) {
                      removeChildren({parentId:'select',childName:'foo'});
                      var modal = document.getElementById('modal');
                      var span = document.getElementsByClassName("close")[0];
                      modal.style.display = 'block';
                      for (var i = 0; i < names.length; i++){
                        $('#select').append('<option name="foo">'+names[i]+'</option>');
                      }
                      span.onclick = function(){
                        $('option').remove();
                        names = [];
                        modal.style.display = 'none';
                      }
                      window.onclick = function(event) {
                      if (event.target == modal) {
                        $('option').remove();
                        names = [];
                        modal.style.display = 'none';
                      }
                    }
                    }
                    if (flag === 0){
                            alert("Ничего не найдено.");
                        }

                });
  });
    $("#search_word").keypress(function(e) {
        if (e.which == 13) {
            $("#search").click();
            return false;
        }
    });

    $('#choose').click(function(){
      var selected = $('#select option:selected').text();
      $.get('markers.xml', function(data){
        a = $(data).find('marker').each(function(){
          var name = $(this).find('name').text();
          var lat = $(this).find('lat').text();
          var lng = $(this).find('lng').text();
          if (name == selected){
            myMap.setCenter([parseFloat(lat), parseFloat(lng)], 16, {checkZoomRange: true});
          }
        });
      });
      modal.style.display = 'none';
      $('option').remove();
      names = [];
    });
});

ymaps.ready(init);
    var myMap;
    var myGeoObjects = [];

    function init(){
      myMap = new ymaps.Map("map", {
            center: [51.533333, 46.000000],
            zoom: 8
        });
        placeMarkers('markers.xml');
        myMap.controls.remove('searchControl');
        myMap.controls.remove('trafficControl');

    };

    function placeMarkers(filename){
      $.get(filename, function(xml){
        $(xml).find("marker").each(function(){
      var name = $(this).find('name').text();
      var address = $(this).find('address').text();

      // create a new LatLng point for the marker
      var lat = $(this).find('lat').text();
      var lng = $(this).find('lng').text();
      var MyBalloonContentLayoutClass = ymaps.templateLayoutFactory.createClass(
    '<h3>'+name+'</h3>'+'<p>Адрес учреждения: '+address+'</p>'+
                '<a class="single_image" href="img/out0002.jpg" data-lightbox="out0002" data-title="'+name+'"><img src="img/out0002.jpg" data-lightbox="out0002" data-title="'+name+'" width="400" height="300" alt="план помещения" class="img-thumbnail"></a>'+
                '<p>Информация о подключении к Интернет: <a href="descr.html">подробнее...</a></p>');

      var marker = new ymaps.Placemark([lat,lng], {
        hintContent: name,
      },
      {
        balloonContentLayout: MyBalloonContentLayoutClass
      });

      myGeoObjects.push(marker);
      //myMap.geoObjects.add(marker);
    });
      var clusterer = new ymaps.Clusterer();
      clusterer.add(myGeoObjects);
      myMap.geoObjects.add(clusterer);
    });
    }

    function removeChildren (params){
    var parentId = params.parentId;
    var childName = params.childName;

    var childNodes = document.getElementById(parentId).childNodes;
    for(var i=childNodes.length-1;i >= 0;i--){
        var childNode = childNodes[i];
        if(childNode.name == 'foo'){
            childNode.parentNode.removeChild(childNode);
        }
    }
}
