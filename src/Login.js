import React from 'react';
import ReactDOM from 'react-dom';
import './Login.css';
import App from './App';
import Cookies from 'universal-cookie';

var axios = require('axios');

export default class LogInPage extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      formValues:{}
    };
  }

  handleChange(event){
    event.preventDefault();
    let formValues = this.state.formValues;
    let name = event.target.name;
    let value = event.target.value;
    formValues[name] = value;
    this.setState({formValues})
  }

  onSave(event){
    event.preventDefault();
    const cookies = new Cookies();
    axios.post(
      'http://192.168.1.125:5000/login',
      this.state.formValues,
      {
      headers: {
          'Access-Control-Allow-Origin': '*'
        }
      },
    ).then((response) => {
      cookies.set('token',response.data.token,{path:'/'});
      //window.sessionStorage.setItem("key", response.data.token);
      let root = document.getElementById('root');
      root.className="new";
      ReactDOM.render(<App token={cookies.get('token')}/>, document.getElementById('root'))
    }
  );
  }

  componentWillMount(){

  }

  render(){
    return(
      <div className="login-page">
        <div className="background-img"></div>
        <div className="form">
          <h2>Авторизация</h2>
          <form className="register-form" onSubmit={this.onSave.bind(this)}>
          <label for="email">E-mail</label>
          <input type="email" className="form-control" name="email" value={this.state.formValues["email"]} onChange={this.handleChange.bind(this)}/>
          <label for="pass">Пароль</label>
          <input type="password" className="form-control" name="password" value={this.state.formValues["password"]} onChange={this.handleChange.bind(this)}/>
          <button type="submit" className="btn btn-success">Войти</button>
        </form>
        </div>
      </div>
    )
  }
}
