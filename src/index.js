import React from 'react';
import ReactDOM from 'react-dom';
import CheckAuth from './CheckAuth';
import Map from './Map';
import Cookies from 'universal-cookie';


const cookies = new Cookies();
ReactDOM.render(

	<CheckAuth token={cookies.get('token')}/>,
  document.getElementById('root')
);
ReactDOM.render(<Map />, document.getElementById('modal'));
