import React, { Component } from 'react';
import Cookies from 'universal-cookie';
import {
  NavLink
} from 'react-router-dom'

export default class FullInfo extends Component{
  componentWillMount() {
    const script = document.createElement("script");

        script.src = "js/fullinfo.js";
        script.async = true;

        document.body.appendChild(script);
  }

  logOut() {
    let cookies = new Cookies();
    cookies.remove('token');
    window.location='/';
  }

  render() {
    return (
    <div style={{width:100+'%', height:100+'%', marginTop:60+'px'}}>

    <div className="navbar navbar-default navbar-fixed-top">
      <div className="navbar-header">
        <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
        </button>
        <a href="/" className="navbar-brand" >Карта</a>

      </div>
      <div className="collapse navbar-collapse">
        <ul className="nav navbar-nav">
          <li className="active">
          <NavLink to="/full-info">Общая информация</NavLink>
          </li>

        </ul>
        <ul className="nav navbar-nav navbar-right">
          <li style={{marginRight:25+'px'}}>
            <div className="btn-nav">
              <button className="btn btn-danger btn-small navbar-btn" onClick={this.logOut}>Выйти</button>
            </div>
          </li>

        </ul>
      </div>
      </div>

    <div className="container" style={{width:75+"%"}}>

        <table id="table" className="table table-hover" style={{display: "none", width: 70+"%", float: "left"}}>
               <thead></thead>
               <tbody></tbody>
               <tfoot></tfoot>
      </table>
      <div className="diagram" style={{float: "left", width: 30+"%", height: 50+"px"}}>
      <canvas id="myChart" width="50" height="50"></canvas>
      </div>

      </div>
      </div>
    )
  }
}
